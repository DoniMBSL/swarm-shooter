export const CONFIG = Object.freeze({
    width: 1080,
    height: 1080,

    player: {
    	style: {
    		stroke: 4,
    		color: 0xFFFFFF
    	},
    	radius: 34,
    	grow: 8
    },

    crosshair: {
    	scale: 0.6,
    	zIndex: 2
    },

    crosshairLine: {
    	active: false,
    },

    enemies: {
    	style: {
    		stroke: 4,
    		explosionColor: 0xFF0000,
    		color: 0x008000,
    	},
    	radius: 18,
    	explosionRadius: 90,
    	speed: 0.7,
    },

    bullets: {
    	style: {
    		stroke: 4,
    		color: 0xFF1010
    	},
    	radius: 10,
    	speed: 6,
    	timer: 6000
    },

});
