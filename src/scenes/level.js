const asset_crosshair = require('../assets/crosshair.png');

import { CONFIG } from '../config'
class Level extends Phaser.Scene {
  constructor() {
    super('Level')
  }

  init() {
  }

  preload() {
    this.load.image('crosshair', asset_crosshair);
  }

  create() {
    this.playerSetup();
    this.crosshairSetup();
    this.enemiesSetup();
    this.bulletsSetup();
    this.inputSetup();
    this.scoreSetup();


    this.counter_enemyCreate = 0;
    this.counter_score = 0;
  }

  playerSetup(){
    this.player = this.add.circle(540, 540, CONFIG.player.radius)
    .setStrokeStyle(CONFIG.player.style.stroke, CONFIG.player.style.color);

    this.player.grow = this.player.radius;
  }
  playerGrow(){
    if (this.player.grow > this.player.radius) {
      this.player.radius++;
    }
  }

  crosshairSetup(){
    this.crosshair = this.add.image(0, 0, 'crosshair')
    .setDepth(CONFIG.crosshair.zIndex)
    .setScale(CONFIG.crosshair.scale)

    if (CONFIG.crosshairLine.active) {
      this.crosshairLine = this.add.line(0, 0, 0, 0)
      .setStrokeStyle(1, 0xFF0000)
      .setOrigin(0, 0)
    }
  }

  enemiesSetup(){
    this.enemies = this.add.group();
    //
  }
  enemyCreate(){
    var x = 0;
    var y = 0;

    // bounds spawn
    if(Math.random() > 0.5){
      x = Phaser.Math.Between(0, CONFIG.width);
      y = ( (Math.random() > 0.5) ? 0 : CONFIG.height);
    } else{
      y = Phaser.Math.Between(0, CONFIG.height);
      x = ( (Math.random() > 0.5) ? 0 : CONFIG.width);
    }

    var enemy = this.add.circle(x, y, CONFIG.enemies.radius, 0x000000)
    .setStrokeStyle(CONFIG.enemies.style.stroke, CONFIG.enemies.style.color);

    var angle = Math.atan( (this.player.x - enemy.x) / (this.player.y - enemy.y) );

    if(enemy.y > this.player.y){
      enemy.speedY = -Math.cos(angle) * CONFIG.enemies.speed;
      enemy.speedX = -Math.sin(angle) * CONFIG.enemies.speed;
    } else{
      enemy.speedX = +Math.sin(angle) * CONFIG.enemies.speed;
      enemy.speedY = +Math.cos(angle) * CONFIG.enemies.speed;
    }
    enemy.explode = false;
    enemy.playerContact = false;

    this.enemies.add(enemy);
  }
  enemiesMove(){
    this.enemies.children.each((child)=>{
      child.x += child.speedX;
      child.y += child.speedY;
    })
  }
  enemiesExplode(enemy){
    this.enemies.children.each((enemy)=>{
      if (enemy.explode) {
        if (enemy.radius > CONFIG.enemies.explosionRadius) {
          this.enemiesDestroy(enemy);
        } else{
          enemy.radius += 4;
        }
      }
    })
  }
  enemiesDestroy(enemy){
    this.scoreUpdate();
    enemy.setActive(false);
    enemy.setVisible(false);
    this.enemies.children.delete(enemy);
  }

  bulletsSetup(){
    this.bullets = this.add.group();
    //
  }
  bulletCreate(pointer){
    var angle = Math.atan( (this.player.x - pointer.x) / (this.player.y - pointer.y) );

    if (pointer.y > this.player.y) {
      var y = this.player.y + (this.player.radius * Math.cos(angle));
      var x = this.player.x + (this.player.radius * Math.sin(angle));
    } else{
      var y = this.player.x - (this.player.radius * Math.cos(angle));
      var x = this.player.y - (this.player.radius * Math.sin(angle));
    }

    var bullet = this.add.circle(
      x,
      y,
      CONFIG.bullets.radius,
      CONFIG.bullets.style.color
    ).setStrokeStyle(CONFIG.bullets.style.stroke, CONFIG.bullets.style.color);

    if(pointer.y > this.player.y){
      bullet.speedY = Math.cos(angle) * CONFIG.bullets.speed;
      bullet.speedX = Math.sin(angle) * CONFIG.bullets.speed;
    } else{
      bullet.speedX = -Math.sin(angle) * CONFIG.bullets.speed;
      bullet.speedY = -Math.cos(angle) * CONFIG.bullets.speed;
    }

    bullet.timer = 0;

    this.bullets.add(bullet);
  }
  bulletsMove(delta){
    this.bullets.children.each((child)=>{
      child.x += child.speedX;
      child.y += child.speedY;

      child.timer += delta;
      if (child.timer > CONFIG.bullets.timer) {
        this.bulletsDestroy(child);
      }
    })
  }
  bulletsDestroy(bullet){
    bullet.setActive(false);
    bullet.setVisible(false);
    this.bullets.children.delete(bullet);
  }

  inputSetup(A, B){
    this.input.on('pointerdown', (pointer) => {
      this.bulletCreate(pointer);
    }, null, this);

    this.input.on('pointermove', (pointer) => {
      this.crosshair.setPosition(pointer.x, pointer.y);
      if (CONFIG.crosshairLine.active) {
        this.crosshairLine.setTo(
          this.player.x,
          this.player.y,
          this.crosshair.x,
          this.crosshair.y
        )
      }
    })
  }

  intersection(A, B){
    var deltaX = A.x - B.x;
    var deltaY = A.y - B.y;
    var distance = Math.sqrt((deltaX*deltaX) + (deltaY*deltaY));

    if (distance < (A.radius + B.radius)) {
      return true;
    } else{
      return false;
    }
  }
  reaction(){
    this.enemies.children.iterate((enemy)=>{

      // bullets
      this.bullets.children.iterate((bullet)=>{
        if (this.intersection(bullet, enemy)) {
          bullet.timer += CONFIG.bullets.timer;
          enemy.explode = true;
          enemy.setStrokeStyle(CONFIG.enemies.style.stroke, CONFIG.enemies.style.explosionColor);
        }
      });

      // enemies
      this.enemies.children.iterate((enemy2)=>{
        if (enemy.explode) {
          if (this.intersection(enemy, enemy2)) {
            enemy2.explode = true;
            enemy2.setStrokeStyle(CONFIG.enemies.style.stroke, CONFIG.enemies.style.explosionColor);
          }
        }
      });

      // player
      if (!enemy.playerContact) {
        if (this.intersection(enemy, this.player)) {
          enemy.playerContact = true;
          this.player.grow += CONFIG.player.grow;
        }
      }
    });

    // enemies cleanup
    this.enemies.children.each((enemy)=>{
      if (enemy.playerContact) {
        this.enemiesDestroy(enemy);
      }
    })
  }

  scoreSetup(){
    this.score = this.add.text(this.player.x, this.player.y, '');
    //
  }
  scoreUpdate(){
    this.counter_score++;
    this.score.setText(this.counter_score);
    Phaser.Display.Align.In.Center(this.score, this.player);
  }

  gameEnd(){
    if (this.player.radius > CONFIG.width/2) {
      alert('Game Over! Score: ' + this.counter_score);
      this.scene.restart('Level')
    }
  }


  update(time, delta) {
    this.gameEnd();
    this.bulletsMove(delta);
    this.enemiesMove();
    this.reaction();
    this.enemiesExplode();
    this.playerGrow();
    this.counter_enemyCreate += delta;
    if (this.counter_enemyCreate > 400) {
      this.counter_enemyCreate = 0;
      this.enemyCreate();
    }

  }


}

export default Level
// can add bullet particles